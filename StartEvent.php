
<?php
session_start();
$db_host = 'localhost'; // Server Name
$db_user = 'root'; // Username
$db_pass = ''; // Password
$db_name = 'orienteering'; // Database Name

$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
if (!$conn) {
	die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

?>


<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/StartEvent.css?">
    <link rel="stylesheet"  type="text/css" href="css/altMenu.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });


    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });


    jQuery( function($) {
        $('tbody tr[data-href]').addClass('clickable').click( function() {
          window.location = $(this).attr('data-href');
        }).find('a').hover( function() {
          $(this).parents('tr').unbind('click');
        }, function() {
          $(this).parents('tr').click( function() {
            window.location = $(this).attr('data-href');
          });
        });
      });
</script>

  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="mainMenu.php">Home</a></li>
        <li><a href="sample.html">MyAccount</a></li>
        <li><a href="sample.html">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href="index.html">About App</a></li>
        <li><a href="index.html">Support</a></li>
      </ul>
    </div>





    <div class="main">
      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>

       <div class="none">
        <div>

        </div>
        <p class="header">Select Your Event</p>

		<?php
		$sql="SELECT  E.* , L.*,C.* FROM event E, location L, course C
			       WHERE C.courseID IN (SELECT courseID FROM course_enroll WHERE userName LIKE '%{$_SESSION['login_user']}%' )
				   AND C.eventID = E.eventID
				   AND E.locationID = L.locationID ORDER BY eventDate";
		$query = mysqli_query($conn, $sql);

		if (!$query) {
		die('Could not get data: ' . mysql_error());
		}

		if (mysqli_num_rows($query)== 0){

       echo "<p Style=\"text-align:center; margin-top:60px;font-size: 20; font-weight: 900; color: #424242;\"> No Event available</p>";
	   echo "<center><button class=\"codebtn\" onclick=\"history.go(-1);\">Back </button></center>";
		}
		else{
		?>

        <table id="Event">
          <thead class = "tableHead">
          <tr>
           <th scope="cols" Style="" >Date</th>
           <th scope="cols" Style="width:23%;padding-left:5px;">Map</th>
           <th scope="cols" Style="width:50%;padding-left:5px;">Start Loction</th>
           <th scope="cols" Style="width:100%;padding-left:5px;">Suburb</th>
          </tr>
		  </thead>
         <tbody>
		<?php
		while ($row = mysqli_fetch_array($query))
		{
			?>
		 <tr class="record"   data-href= "StartED.php?id= <?php echo $row['courseID'];?>">
			<td><?php echo date('d M', strtotime($row['eventDate'])); ?></td>
			<td><?php echo $row['eventName'];?></td>
			<td><?php echo $row['address'];?></td>
			<td><?php echo $row['suburb'];?></td>
			</tr>
		<?php } ?>
		</tbody>
        </table>

		<?php }?>

    </div><!-- /#main -->
	</div>

 </body>
</html>
