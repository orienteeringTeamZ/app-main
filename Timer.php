<?php
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering';

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);

$startCode = $_SESSION["startCode"];
$courseID = $_SESSION["courseID"];
$number = $_SESSION["numberOfpoint"];
$type = $_SESSION["type"];

date_default_timezone_set("Australia/Melbourne");
?>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/Timer.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });
 </script>

 <script>
 		window.console = window.console || function(t) {};
 		window.open = function(){ console.log('window.open is disabled.'); };
 		window.print = function(){ console.log('window.print is disabled.'); };
 		if (false) {
 		  window.ontouchstart = function(){};
 		}
 	</script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });



    </script>

  <script type='text/javascript'>
$(document).ready(function(){
    $(".codecontent").hide();
    var flg = "close";
    $(".codebtn").click(function(){
        $(".codecontent").slideToggle();
        if(flg == "close"){
            $(this).text("Type Event Code");
            flg = "open";
        }else{
            $(this).text("Start Course");
            flg = "close";
        }
    });
});


</script>


<script type="text/javascript">
<!--
window.onload = initialize;

function initialize() {
    document.getElementById( 'stopwatchStartAndStop' ).onclick=stopwatch;
    document.getElementById( 'stopwatchClear' ).onclick=stopwatchClear;
}

var $enable = false, $stopwatchTime, $startTime, $stopwatchTimeAdd = 0;
function stopwatch() {
    $enable = !$enable;
    if( $enable ){
        stopwatchStart();
    }else{
        stopwatchStop();
    }
}
function stopwatchStart() {
    //document.getElementById( 'stopwatchStartAndStop' ).innerHTML= 'Started';
    if( $startTime === undefined ){
        var $startDate = new Date();
        $startTime = $startDate.getTime();
    }
    var $nowDate = new Date();
    $stopwatchTime = $nowDate.getTime() - $startTime + $stopwatchTimeAdd;
    $stopwatchMillisecond = $stopwatchTime % 1000;
    $stopwatchSecond = Math.floor( $stopwatchTime / 1000 ) % 60;
    $stopwatchMinute = Math.floor( $stopwatchTime / 1000 / 60 ) % 60;
    $stopwatchHour = Math.floor( Math.floor( $stopwatchTime / 1000 / 60 ) / 60 );
    if( $stopwatchMillisecond < 10 ){
        $stopwatchMillisecond = '0' + $stopwatchMillisecond;
    }
    if( $stopwatchMillisecond < 100 ){
        $stopwatchMillisecond = '0' + $stopwatchMillisecond;
    }
    if( $stopwatchSecond < 10 ){
        $stopwatchSecond = '0' + $stopwatchSecond;
    }
    if( $stopwatchMinute < 10 ){
        $stopwatchMinute = '0' + $stopwatchMinute;
    }
    if( $stopwatchHour < 10 ){
        $stopwatchHour = '0' + $stopwatchHour;
    }
	if( $stopwatchMinute == 50 ){
        stopwatchStop();
		window.location="checkResult.php";
    }
    //document.getElementById( 'stopwatchHour' ).innerHTML= $stopwatchHour;
    document.getElementById( 'stopwatchMinute' ).innerHTML= $stopwatchMinute;
    document.getElementById( 'stopwatchSecond' ).innerHTML= $stopwatchSecond;
    //document.getElementById( 'stopwatchMillisecond' ).innerHTML= $stopwatchMillisecond;
    $stopwatch = setTimeout( "stopwatchStart()", 1 );
}
function stopwatchStop() {
    //document.getElementById( 'stopwatchStartAndStop' ).innerHTML= 'Start';
    clearTimeout( $stopwatch );
    $startTime = undefined;
    $stopwatchTimeAdd = $stopwatchTime;
}
function stopwatchClear() {
    $startTime = undefined;
    $stopwatchTimeAdd = 0;
    //document.getElementById( 'stopwatchHour' ).innerHTML= '00';
    document.getElementById( 'stopwatchMinute' ).innerHTML= '00';
    document.getElementById( 'stopwatchSecond' ).innerHTML= '00';
    //document.getElementById( 'stopwatchMillisecond' ).innerHTML= '000';
}

var cnt = 0;
$(function() {
$(".submit").click(function() {
var codeInput = $("#CCode").val();
var dataString = 'CCode='+ codeInput;
if(codeInput=='')
{
alert("Enter check code ...");
$("#CCode").focus();
}
else
{
	if(cnt == 0){
		if (codeInput=="<?php echo $startCode;?>")
		{
			stopwatchStart();
			cnt=parseInt(cnt)+parseInt(1);
			$("#flash").show();
			$("#flash").fadeIn(400).html('<span class="load">Loading..</span>');
			$.ajax({
				type: "POST",
				url: "addCode.php",
				data: dataString,
				cache: true,
				success: function(html){
					$("#show").after(html);
					document.getElementById('CCode').value='';
					$("#flash").hide();
					$("#CCode").focus();
				}
			});
		}
		else{
			alert("Wrong start code input !!!");
			return false;
		}
	}
	else
	{
		cnt=parseInt(cnt)+parseInt(1);

		$("#flash").show();
			$("#flash").fadeIn(400).html('<span class="load">Loading..</span>');
			$.ajax({
				type: "POST",
				url: "addCode.php",
				data: dataString,
				cache: true,
				success: function(html){
					$("#show").after(html);
					document.getElementById('CCode').value='';
					$("#flash").hide();
					$("#CCode").focus();
				}
			});

		if (cnt > parseInt(<?php echo $number;?>))
		{
			window.location="checkResult.php";
		}
		return false;
	}

}
return false;
});
});


</script>
  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="sample.html">MyAccount</a></li>
        <li><a href="sample.html">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href="index.html">About App</a></li>
        <li><a href="index.html">Support</a></li>
      </ul>
    </div>

    <div class="main">

      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>

       <div class="none">
         <div id="stopwatchWrapper">
           <div id="stopwatch">
           <span id="stopwatchMinute">00</span>
           <span>:</span>
           <span id="stopwatchSecond">00</span>
           <!--<span>.</span>-->
           <!--<span id="stopwatchMillisecond">00</span>-->
           </div>
      <!-- <div id="stopwatchControl">
           <button id="stopwatchStartAndStop" class="button">Start</button>
       </button id="stopwatchClear" class="Clear">Clear</button>
     </div>-->
   </div>
<center>
   <div id="wrap">
     <table >
	 <?php
		$sql="SELECT  E.* , L.* , C.* FROM event E, location L , course C
			       WHERE C.courseID = '{$courseID}'
				    AND C.eventID = E.eventID
				   AND E.locationID = L.locationID";
		$query = mysqli_query($conn, $sql);

		if (mysqli_num_rows($query)== 0){
			echo "No data available for that name specified";
		}
		else{
			while ($row = mysqli_fetch_array($query))
			{
			?>
       <tr>
         <td>Event Name: </td>
         <td> <?php echo $row['eventName'];?> </td>
       </tr>
       <tr>
         <td>Course Type: </td>
         <td> <?php echo $row['courseType']; ?> - <?php echo $row['courseDuration']." mins"; ?> </td>
       </tr>
		<?php }}?>
     </table>
	 <form name="myForm" method="post" >
     <ul>
       <li><input id ="CCode" class="CCode" type="text" name="CCode"  placeholder="Type Check code"/></li>
       <li><input id = "submit" class="submit" name="submit"  type="submit" value="Check" /></li>
     </ul>
	 </form>
   </div>
  </center>

   <center>
   <div id="flash"></div>
	<div id="show"></div>
   <a href="Retire.php"><button class="retireB">Retire</button>
   <!-- <p id="display"></p> -->
   </center>



   </div><!-- /#displaynon -->
    </div><!-- /#main -->

 </body>
</html>
