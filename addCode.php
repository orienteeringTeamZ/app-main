<?php  
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering'; 

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);

$startCode = $_SESSION["startCode"];
$courseID = $_SESSION["courseID"];
$number = $_SESSION["numberOfpoint"];
$type = $_SESSION["type"];

date_default_timezone_set("Australia/Melbourne"); 
$sql ="INSERT INTO `participate` (`userName`, `courseID`, `time`, `pointAcquired`, `insertCode`, `order`, `status`) VALUES (?,?,?,?,?,?,?)";

if($_SESSION["count"]==0)
{
	$q = $db->prepare($sql);
	$q->execute(array($_SESSION["login_user"],$courseID,date('G:i:s'),0,$_POST['CCode'],$_SESSION["count"],'Start'));
	$_SESSION["count"] = $_SESSION["count"]+1;
}
else
{
	
	if(($type == "Line") || ($type == "Scatter"))
	{
		$q1 = $db->prepare("SELECT pointCode 
							from controlpoint 
							where eventID = (select eventID from course where courseID = '{$courseID}')
							and lineOrder = '{$_SESSION['count']}'");
		$q1->execute();
		$row = $q1->fetch();
		
		if($_SESSION["count"]==$number)
		{
			if($_POST['CCode'] == $row['pointCode'])
			{
				$q2 = $db->prepare("SELECT *
							from participate 
							where courseID = '{$courseID}' and userName ='{$_SESSION['login_user']}'
							and status ='wrong'");
				$q2->execute();
				$row2 = $q2->fetch();
				if($row2 == 0)
				{
					$status = "finish";
				}
				else
				{
					$status = "fail";
				}
				
			}
			else
			{
				$status = "fail";
			}	
		}
		else
		{
			if($_POST['CCode'] == $row['pointCode'])
			{
				$status = "correct";
			}
			else
			{
				$status = "wrong";;
			}
		}
		
		$q = $db->prepare($sql);
		$q->execute(array($_SESSION["login_user"],$courseID,date('G:i:s'),0,$_POST['CCode'],$_SESSION["count"],$status));
		$_SESSION["count"] = $_SESSION["count"]+1;
	}
	else
	{
		
	}

}	

			
?>