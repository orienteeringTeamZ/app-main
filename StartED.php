
<?php
include('session.php');

$db_host = 'localhost'; // Server Name
$db_user = 'root'; // Username
$db_pass = ''; // Password
$db_name = 'orienteering'; // Database Name

$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
if (!$conn) {
	die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

?>

<html lang="ja">
<?php
$id=$_GET['id'];
?>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/StartED.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });

	function initialize() {
    document.getElementById( 'submit' ).onclick=setID;
	function setID()
	{
    <?php $_SESSION["courseID"] = $id;?>
	<?php
	$query2 = mysqli_query($conn, "SELECT * from course where courseID ='{$id}'");
	if (!$query2)
	{
		die('Could not get data: ' . mysql_error());
	}

	if (mysqli_num_rows($query2)== 0)
	{

		echo "No data available for that name specified";
	}
	else{
		$row2 = mysqli_fetch_array($query2);
		$_SESSION["startCode"] = $row2['startCode'];
		$_SESSION["numberOfpoint"] = $row2['nrOfcontrolPoint'];
		$_SESSION["type"] = $row2['courseType'];
		$_SESSION["count"] = 0;
	}
	?>
    }
	}

 </script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });
    </script>
  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="sample.html">MyAccount</a></li>
        <li><a href="sample.html">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href="index.html">About App</a></li>
        <li><a href="index.html">Support</a></li>
      </ul>
    </div>

    <div class="main">

      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>

       <div class="none">

      <div class="EventD" >
      <div Class="eventTop"></div>
       <div Class="eventContent">
        <?php
		$sql="SELECT  E.* , L.* , C.* FROM event E, location L , course C
			       WHERE C.courseID = ".$id.
				   " AND C.eventID = E.eventID
				   AND E.locationID = L.locationID";
		$query = mysqli_query($conn, $sql);

		if (mysqli_num_rows($query)== 0){
			echo "No data available for that name specified";
		}
		else{
			while ($row = mysqli_fetch_array($query))
			{
			?>
				<p class="eventTitle"><?php echo $row['eventName']; ?></p>
				<div Style="margin-left:20px; margin-top:20px; margin-bottom:30px; font-weight:600; "  >
				<table id="table1">
				<tr>
				<td>Date Time:</td><td Style="padding-left:10px;"><?php echo date('d F o', strtotime($row['eventDate']));?> , <?php echo date('G:i', strtotime($row['eventDuration']));?></td></tr>
				<tr><td>Start Location:</td><td Style="padding-left:10px;"><?php echo $row['address']; ?></tr>
				<tr><td>Suburb:</td><td Style="padding-left:10px;"><?php echo $row['suburb']; ?></td></tr>
				<tr><td>Melway:</td><td Style="padding-left:10px;"><?php echo $row['melway']; ?></td></tr>
				<tr><td>Event Type:</td><td Style="padding-left:10px;"><?php echo $row['courseType']; ?> - <?php echo $row['courseDuration']." mins"; ?></td></tr>
            <tr><td>Controls:</td><td Style="padding-left:10px;"><?php echo $row['nrOfcontrolPoint']; ?></td></tr>

			<?php
			}
		}
			?>
           </table>

           <table id="note">
             <tr class="note"><th>Additional Note</th></tr>
             <tr><td><p>Please Wear comfortable Clothes and shoes</P></td></tr>

           </table>
           </div>



      <div Style=" margin-top:20px; margin-bottom:30px;">
      <center>
		  <a href="Groupcheck.php"><button id = "submit" class="start">Start Course</button></a>
      <button class="codebtn" onclick="history.go(-1);">Back </button>
      <a href ='deleteCourse.php?id=<?php echo $id;?>'><button class="codebtn">Cancel</button></a>


     </center>
      </div>


      </div>
      <div style="text-align:center;margin-bottom:30px;">
      <img Style="Width:95%" Src="images/slideImage.jpg">
      </div>
     </div>


     </div>

    </div><!-- /#main -->

 </body>
</html>
