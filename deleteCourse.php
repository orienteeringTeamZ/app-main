<?php
	session_start();
	$db_host		= 'localhost';
	$db_user		= 'root';
	$db_pass		= '';
	$db_database	= 'orienteering'; 

	$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);
	
	$userName = $_SESSION['login_user'];
	$id = isset($_GET['id']) ? $_GET['id'] : '';
	
	$sql = "DELETE FROM course_enroll WHERE userName LIKE '%{$userName}%' AND courseID = '{$id}'";
	$result = $db->prepare($sql);
	$result->execute();
	Header( 'Location: StartEvent.php' );
?>