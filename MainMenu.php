<?php
include('session.php');
?>

<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/MainMenu.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });
    </script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });
    </script>



  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="AccountSet.php">MyAccount</a></li>
        <li><a href="SearchEvent.php">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href=".php">About App</a></li>
        <li><a href=".php">Support</a></li>
      </ul>
    </div>





  <div id="main">
   <p class="username"><?php echo $login_session; ?></p>
     <p class="welcome"> Welecome to the <br>Orienteering Victoria App</p>
     <p Style="text-align:center; margin-top:20px;">
        <a href="logout.php"><input class="logout" type="submit"  value="Log Out"  /></a></p>

	<p Style="text-align:center; margin-top:60px;font-size: 20; font-weight: 900; color: blue;">
		<?php
			if ( isset($_GET['success']) && $_GET['success'] == 1 )
			{
			// treat the succes case ex:
					echo "<img Style=\"width:80%;\" Src='images/registeredMessage.png'/>";
			}else if(( isset($_GET['success']) && $_GET['success'] == 0 ))
			{
				echo "You have already erolled this course !!!";
			}
		?>
	</p>
<table class="mib"  >
    <tr>
      <td class="mi"><a href="manageAccount.php"><img class="mi" Src="images/account.png"/></a></td>
      <td class="mi"><a href="search.php"><img class="mi" Src="images/serEve.png"/></a></td>
    <td Class="mi"><a href="StartEvent.php">
         <img class="mi" Src="images/startcourse.png" type="submit"/></a></td>
    </tr>
</table>



    </div><!-- /#main -->





 </body>
</html>
