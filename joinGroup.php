<?php
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering'; 

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);
			
			

$sql = "INSERT INTO joinedGroup (userName,groupName) VALUES (?,?)";
$q = $db->prepare($sql);
$q->execute(array($_SESSION["login_user"],$_GET['groupName']));
Header( 'Location: groupcheck.php?group=1' );