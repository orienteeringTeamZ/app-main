<?php  
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering'; 

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);
 
 
 
if (isset($_POST['groupName']) ){
	
	$groupName = $_POST['groupName'];
	$courseID = $_SESSION["courseID"];
	$sql1= "SELECT * FROM courseGroup WHERE groupName = '{$groupName}' AND courseID = '{$courseID}' ";
	$query = mysqli_query($conn,$sql1);
	if (!$query) 
	{ 
		echo("Error description: " . mysqli_error($conn));
	}
	else
	{
		$row = mysqli_num_rows($query);
		if ($row == 0)
		{
			$sql = "INSERT INTO courseGroup (groupName,courseID) VALUES (?,?)";
			$sql2 = "INSERT INTO joinedGroup (userName,groupName) VALUES (?,?)";
			$q = $db->prepare($sql);
			$q->execute(array($groupName,$courseID));
			$q1 = $db->prepare($sql2);
			$q1->execute(array($_SESSION["login_user"],$groupName));
			Header( 'Location: groupcheck.php?group=1' );
		}
		else
		{
			header('location: groupcheck.php?group=0');
		}
	}

}
?>