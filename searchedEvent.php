
<?php
$db_host = 'localhost'; // Server Name
$db_user = 'root'; // Username
$db_pass = ''; // Password
$db_name = 'orienteering'; // Database Name

$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
if (!$conn) {
	die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

?>


<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/StartEvent.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });
    </script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });


    jQuery( function($) {
        $('tbody tr[data-href]').addClass('clickable').click( function() {
          window.location = $(this).attr('data-href');
        }).find('a').hover( function() {
          $(this).parents('tr').unbind('click');
        }, function() {
          $(this).parents('tr').click( function() {
            window.location = $(this).attr('data-href');
          });
        });
      });
    </script>
<style>
table {
    border-collapse: collapse;
    width: 100%;
	background: #dff0d8;
}



tr:nth-child(even){background-color: #f2f2f2}
</style>


  </head>
<body>


  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="AccountSet.php">MyAccount</a></li>
        <li><a href="SearchEvent.php">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href=".php">About App</a></li>
        <li><a href=".php">Support</a></li>
      </ul>
    </div>

<?php
$sql = 'SELECT E.*, L.*
		FROM event E, location L
		where E.locationID = L.locationID';



	if(isset($_POST['courseType']))
	{
        if(!empty($_POST['courseType']))
		{

			$courseType= $_POST['courseType'];

			 $sql="SELECT  E.* , L.*,C.* FROM event E, location L, course C
			       WHERE E.locationID = L.locationID
                   AND 	C.eventID = E.eventID AND C.courseType LIKE '%{$courseType}%'";


			if(isset($_POST['eventName']))
			{
				if(!empty($_POST['eventName']))
				{
					$sql.=" AND E.eventName LIKE '%{$_POST['eventName']}%'";
				}

			}

			if(isset($_POST['suburb']))
			{
				if(!empty($_POST['suburb']))
				{
					$sql.=" AND L.suburb LIKE '%{$_POST['suburb']}%'";
				}

			}

			if(isset($_POST['eventDate']))
			{
				if(!empty($_POST['eventDate']))
				{
					$eventDate= $_POST['eventDate'];

					$sql.=" AND DATE_FORMAT(eventDate, '%Y %m %d') = DATE_FORMAT('$eventDate', '%Y %m %d') ";
				}
			}

			if(isset($_POST['eventTime']))
			{
				if(!empty($_POST['eventTime'])){
					if($_POST['eventTime'] == 1)
					{
						$sql .=" AND eventDuration BETWEEN '06:00:00' AND '12:00:00'";
					}
					else if($_POST['eventTime'] == 2)
					{
						$sql .=" AND eventDuration BETWEEN '12:00:00' AND '18:00:00'";
					}
					else
					{
						$sql .=" AND eventDuration BETWEEN '18:00:00' AND '22:00:00'";
					}
				}
            }


			 $sql.=" ORDER BY eventDate";
        }
		else
		{
			if(isset($_POST['eventName']))
			{
				if(!empty($_POST['eventName']))
				{
					$sql .= " AND E.eventName LIKE '%{$_POST['eventName']}%'";
				}
			}

			if(isset($_POST['suburb']))
			{
				if(!empty($_POST['suburb']))
				{
					$sql .= " AND L.suburb LIKE '%{$_POST['suburb']}%'";
				}

			}

			if(isset($_POST['eventDate']))
			{
				if(!empty($_POST['eventDate']))
				{
					$eventDate= $_POST['eventDate'];
					$sql.=" AND DATE_FORMAT(eventDate, '%Y %m %d') = DATE_FORMAT('$eventDate', '%Y %m %d') ORDER BY eventDate ";
				}

			}

			if(isset($_POST['eventTime']))
			{
				if(!empty($_POST['eventTime'])){
					if($_POST['eventTime'] == 1)
					{
						$sql .=" AND eventDuration BETWEEN '06:00:00' AND '12:00:00'";
					}
					else if($_POST['eventTime'] == 2)
					{
						$sql .=" AND eventDuration BETWEEN '12:00:00' AND '18:00:00'";
					}
					else
					{
						$sql .=" AND eventDuration BETWEEN '18:00:00' AND '22:00:00'";
					}
					}
			}

			$sql.=" ORDER BY eventDate";
		}
	}
		$query = mysqli_query($conn, $sql);

if (!$query) {
	 die('Could not get data: ' . mysql_error());
}

if (mysqli_num_rows($query)== 0){

       echo "No data available for that name specified";
}
else{
?>

<div class="main">
      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>

       <div class="none">
        <div>

        </div>
        <p class="header"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Search Event</p>

        <table id="Event">
          <thead class = "tableHead">
          <tr >
           <th scope="cols" Style="" >Date</th>
           <th scope="cols" Style="width:40%;padding-left:5px;">Event Name</th>
           <th scope="cols" Style="width:40%;padding-left:5px;">Start Loction</th>
           <th scope="cols" Style="width:100%;padding-left:5px;">Suburb</th>
          </tr>
        </thead>
    <tbody>
<?php
		while ($row = mysqli_fetch_array($query))
		{
			?>
		 <tr class="record"   data-href= "courseEnroll.php?id= <?php echo $row['eventID'];?>">
			<td><?php echo date('d M', strtotime($row['eventDate'])); ?></td>
			<td><?php echo $row['eventName'];?></td>
			<td><?php echo $row['address'];?></td>
			<td><?php echo $row['suburb'];?></td>
			</tr>
		<?php } ?>
		<?php } ?>
	</tbody>
         </table>
          </div>
      </div>

 </body>
</html>
