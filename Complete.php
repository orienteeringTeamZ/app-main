<?php
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering';

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);

$q = $db->prepare("select * from ranking
					where userName ='{$_SESSION['login_user']}'
					and courseID = '{$_SESSION['courseID']}'");
$q->execute();
$row1 = $q->fetch();
$status = $row1['status'];
$time = $row1['time'];


?>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/Complete.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu2.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });
 </script>

 <script>
 		window.console = window.console || function(t) {};
 		window.open = function(){ console.log('window.open is disabled.'); };
 		window.print = function(){ console.log('window.print is disabled.'); };
 		if (false) {
 		  window.ontouchstart = function(){};
 		}
 	</script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });



    </script>

  <script type='text/javascript'>
$(document).ready(function(){
    $(".codecontent").hide();
    var flg = "close";
    $(".codebtn").click(function(){
        $(".codecontent").slideToggle();
        if(flg == "close"){
            $(this).text("Type Event Code");
            flg = "open";
        }else{
            $(this).text("Start Course");
            flg = "close";
        }
    });
});
</script>





  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="sample.html">MyAccount</a></li>
        <li><a href="sample.html">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href="index.html">About App</a></li>
        <li><a href="index.html">Support</a></li>
      </ul>
    </div>





    <div class="main">
      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>

       <div class="none">
        <div class="succsessW">
          <div class="succsess">
		  <?php
		  if($status == "fail")
		  {?>
			<p style="text-align:center;padding-top:20px; ">FAILED</p>
          </div>
          <div class="timeW">
		  <p Style="text-align:center; margin-top:0px;font-size: 14; font-weight: 900; color: blue;">You missed some control point(s) or typed wrong code(s)...</p>
		</div>
		 <?php }
		  else
		  { ?>
				<p style="text-align:center;padding-top:20px; ">Congratuation!</p>
          </div>
          <div class="timeW">
          <table class=""><tr><td>
          <p style="padding-top:20px; font-size:26px; font-weight:bold; color:#363636;padding: 40px 0px 0px 20px;">Your Time:</p></td>
          <td><p style="padding-top:20px; font-size:26px; font-weight:bold; color:#363636;padding: 40px 0px 0px 40px;"><?php echo $time; ?></p></td>
          </table>
          </div>
		  <?php }
		  ?>


       </div>
       <center>
       <p><a href="Ranking.php"><button class="backhome"> See Ranking </button></a></p>
     </center>
 </div>
    </div><!-- /#main -->











 </body>
</html>
