
<?php
$db_host = 'localhost'; // Server Name
$db_user = 'root'; // Username
$db_pass = ''; // Password
$db_name = 'orienteering'; // Database Name

$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
if (!$conn) {
	die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

?>

<html lang="ja">
<?php
$id=$_GET['id'];
?>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->
    <link rel="stylesheet" href="css/StartED.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });
 </script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });


	 function typeChange(selectObj) {
	// get the index of the selected option
	var idx = selectObj.selectedIndex;
	// get the value of the selected option
	var which = selectObj.options[idx].value;
	// use the selected option value to retrieve the list of items from the countryLists array
	cList = countryLists[which];
	// get the country select element via its known id
	var cSelect = document.getElementById("n");
	// remove the current options from the country select
	var len=cSelect.options.length;
	while (cSelect.options.length > 0) {
	cSelect.remove(0);
	}
	var newOption;
	// create new options
	for (var i=0; i<cList.length; i++) {
	newOption = document.createElement("option");
	newOption.value = cList[i];  // assumes option string and value are the same
	newOption.text=cList[i];
	// add the new option
	try {
	cSelect.add(newOption);  // this will fail in DOM browsers but is needed for IE
	}
	catch (e) {
	cSelect.appendChild(newOption);
	}
	}
	}
    </script>
  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="sample.html">MyAccount</a></li>
        <li><a href="sample.html">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href="index.html">About App</a></li>
        <li><a href="index.html">Support</a></li>
      </ul>
    </div>

    <div class="main">

      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>

       <div class="none">
        <form action="participate.php" method="post">
      <div class="EventD" >
      <div Class="eventTop"></div>
       <div Class="eventContent">
		<?php
		$sql="SELECT  E.* , L.* FROM event E, location L
			       WHERE E.eventID = ".$id.
				   " AND E.locationID = L.locationID";
		$query = mysqli_query($conn, $sql);

		if (mysqli_num_rows($query)== 0){
			echo "No data available for that name specified";
		}
		else{
			while ($row = mysqli_fetch_array($query))
			{
			?>
				<p class="eventTitle"><?php echo $row['eventName']; ?></p>
				<div Style="margin-left:20px; margin-top:20px; margin-bottom:30px; font-weight:600; "  >
				<table id="table1">
				<tr>
				<td>Date Time:</td><td Style="padding-left:10px;"><?php echo date('d F o', strtotime($row['eventDate']));?> , <?php echo date('G:i', strtotime($row['eventDuration']));?></td></tr>
				<tr><td>Start Location:</td><td Style="padding-left:10px;"><?php echo $row['address']; ?></tr>
				<tr><td>Suburb:</td><td Style="padding-left:10px;"><?php echo $row['suburb']; ?></td></tr>
				<tr><td>Melway:</td><td Style="padding-left:10px;"><?php echo $row['melway']; ?></td></tr>
				<tr><td>Event Type:</td><td Style="padding-left:10px;">

				<select name="courseType" class="selectBox">
				<?php
					$sql1="SELECT * FROM course WHERE eventID = ".$id;
					$query1 = mysqli_query($conn, $sql1);

					if (mysqli_num_rows($query1)== 0){

					echo "No data available for that name specified";
					}
					else{
						while ($row1 = mysqli_fetch_array($query1))
						{
						?>
							<option value="<?php echo $row1['courseID']; ?>"><?php echo $row1['courseType']; ?> - <?php echo $row1['courseDuration']." mins"; ?></option>
						<?php
						}
					}
				?>
				</select></td></tr>
				<tr><td>Controls:</td><td Style="padding-left:10px;">20</td></tr>
				</table>
				<table id="note">
				<tr class="note"><th>Additional Note</th></tr>
				<tr><td><p>Please Wear comfortable Clothes and shoes</P></td></tr>
				</table>
			</div>



      <div Style=" margin-top:20px; margin-bottom:30px;">
      <center>
      <button class="codebtn" onclick="history.go(-1);">Back </button>
      <button class="codebtn" type="submit">Participate</button>
     </center>
	 <?php
		}
		?>
		 <?php
		}
		?>
		</form>
      </div>


      </div>
      <div style="text-align:center;margin-bottom:30px;">
      <img Style="Width:95%" Src="images/slideImage.jpg">
      </div>
     </div>


     </div>

    </div><!-- /#main -->

 </body>
</html>
