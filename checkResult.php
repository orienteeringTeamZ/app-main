<?php  
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering'; 

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);

$startCode = $_SESSION["startCode"];
$courseID = $_SESSION["courseID"];
$number = $_SESSION["numberOfpoint"];
$type = $_SESSION["type"];

$q1 = $db->prepare("select c.groupName from courseGroup c, joinedGroup j
					where c.groupName = j.groupName
					and j.userName = '{$_SESSION['login_user']}'
					and c.courseid = '{$courseID}'");
$q1->execute();
$row1 = $q1->fetch();
$groupname = $row1['groupName'];


$sql = "SELECT * FROM `participate` WHERE userName = '{$_SESSION['login_user']}' and courseID ='$courseID' and status = 'finish'";
$q = $db->prepare($sql);

	$q->execute();
	$row = $q->fetch();
	if($row != 0)
	{
		$status = "success";
		$q3 = $db->prepare("SELECT time FROM `participate` WHERE userName = '{$_SESSION['login_user']}' and courseID ='$courseID' and status ='start'");
		$q3->execute();
		$row3 = $q3->fetch();
		$startTime = date('G:i:s', strtotime($row3['time']));
		
		$q4 = $db->prepare("SELECT time FROM `participate` WHERE userName = '{$_SESSION['login_user']}' and courseID ='$courseID' and status ='finish'");
		$q4->execute();
		$row4 = $q4->fetch();
		$finishTime = date('G:i:s', strtotime($row4['time']));
		
		$q5 = $db->prepare("SELECT timediff('{$finishTime}','{$startTime}') as duration");
		$q5->execute();
		$row5 = $q5->fetch();
		$duration = date('G:i:s', strtotime($row5['duration']));
		
		$q6 = $db->prepare("INSERT INTO `ranking` (`courseID`, `groupName`, `userName`, `time`, `score`,`status`) VALUES (?,?,?,?,?,?)");
		$q6->execute(array($courseID, $groupname, $_SESSION['login_user'], $duration, 0, $status));
		
	}
	else
	{
		$status = "fail";
		$q6 = $db->prepare("INSERT INTO `ranking` (`courseID`, `groupName`, `userName`, `time`, `score`,`status`) VALUES (?,?,?,?,?,?)");
		$q6->execute(array($courseID, $groupname, $_SESSION['login_user'], '', '', $status));
	}

header('location: Complete.php');




?>