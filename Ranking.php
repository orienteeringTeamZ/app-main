<?php
 session_start();
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'orienteering';

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_database);

$q = $db->prepare("select * from ranking
					where userName ='{$_SESSION['login_user']}'
					and courseID = '{$_SESSION['courseID']}'");
$q->execute();
$row1 = $q->fetch();
$groupname = $row1['groupName'];


?>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OrienteeringVictoria</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"> </script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Css links -->

    <link rel="stylesheet" href="css/Ranking.css">
    <link rel="stylesheet"  type="text/css" href="css/altMenu2.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle").click(function(){
        $("#menu").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#menu").show();
        }
      });
    });
 </script>

 <script>
 		window.console = window.console || function(t) {};
 		window.open = function(){ console.log('window.open is disabled.'); };
 		window.print = function(){ console.log('window.print is disabled.'); };
 		if (false) {
 		  window.ontouchstart = function(){};
 		}
 	</script>

    <script type='text/javascript'><!-- javascriptMenu -->
    $(function(){
      $("#toggle2").click(function(){
        $("#md").slideToggle();
        return false;
      });
      $(window).resize(function(){
        var win = $(window).width();
        var p = 480;
        if(win > p){
          $("#md").show();
        }
      });
    });



    </script>

  <script type='text/javascript'>
$(document).ready(function(){
    $(".codecontent").hide();
    var flg = "close";
    $(".codebtn").click(function(){
        $(".codecontent").slideToggle();
        if(flg == "close"){
            $(this).text("Type Event Code");
            flg = "open";
        }else{
            $(this).text("Start Course");
            flg = "close";
        }
    });
});
</script>






  </head>
<body>

  <div id="m2">
    <div id="toggle2"><a href="#"><img class="titlelogo" Src="images/titleLogo5.png"/></a></div>
     <ul id="md">
        <li><a href="MainMenu.php">Home</a></li>
        <li><a href="sample.html">MyAccount</a></li>
        <li><a href="sample.html">Event</a></li>
        <li><a href="StartEvent.php">StartEvent</a></li>
        <li><a href="index.html">About App</a></li>
        <li><a href="index.html">Support</a></li>
      </ul>
    </div>





    <div class="main">
      <p class="snone noAvilable" style="font-size:16; font-weight:600">
      This Feature is avilable on Smart Phone.
     </p>
       <div class="none">


        <div id="rankingW">
          <div id="ranking" class="scroll">
            <div class="tab-content">
              <div id="signup" >
			  <?php
					$sql = "SELECT username,groupname,time, @curRank := @curRank + 1 AS rank
							FROM ranking ra, (SELECT @curRank := 0) r
							where courseID = '{$_SESSION['courseID']}' and status ='success'
							ORDER BY  time";
					$query = mysqli_query($conn, $sql);
					if (!$query) {
					die('Could not get data: ' . mysql_error());
					}
					if (mysqli_num_rows($query)== 0){
						echo "No data available";
					}
					else{
					?>

              <h1 Style="margin-left:20px;">Ranking</h1>
              <table Style="border-bottom:10px;">
                <tr Style="">
                 <th scope="cols" Style="padding-left:5px;" >Rank</th>
                 <th scope="cols" Style="width:40%;padding-left:20px;">Name</th>
                 <th scope="cols" Style="width:40%;padding-left:20px;">Group</th>
                 <th scope="cols" Style="width:100%;">Time</th>
                </tr Style="background-color: #f2f2f2;">
				<?php while ($row = mysqli_fetch_array($query))
					{
					?>
				<tr>
                   <td Style="text-align:center; "><?php echo $row['rank'];?></td>
                   <td Style="padding-left:20px;"><?php echo $row['username'];?></td>
                   <td Style="padding-left:20px;"><?php echo $row['groupname'];?></td>
                   <td><?php echo $row['time']; ?></td>
                 </tr>
					<?php }}?>
               </table>

               <center>
               <p><a href=".php"><button class="download"> Download </button></a></p>
               </center>

              </div>



              <div id="login">

              <h1 Style="margin-left:20px;"><?php echo $groupname;?></h1>
              	  <?php
					$sql = "SELECT username,groupname,time, @curRank := @curRank + 1 AS rank
							FROM ranking ra, (SELECT @curRank := 0) r
							where courseID = '{$_SESSION['courseID']}' and groupName = '{$groupname}'  and status ='success'
							ORDER BY  time";
					$query = mysqli_query($conn, $sql);
					if (!$query) {
					die('Could not get data: ' . mysql_error());
					}
					if (mysqli_num_rows($query)== 0){
						echo "No data available";
					}
					else{
					?>

              <table Style="border-bottom:10px;">
                <tr Style="">
                 <th scope="cols" Style="padding-left:5px;" >Rank</th>
                 <th scope="cols" Style="width:40%;padding-left:20px;">Name</th>
                 <th scope="cols" Style="width:40%;padding-left:20px;">Group</th>
                 <th scope="cols" Style="width:100%;">Time</th>
                </tr Style="background-color: #f2f2f2;">
				<?php while ($row = mysqli_fetch_array($query))
					{
					?>
				<tr>
                   <td Style="text-align:center; "><?php echo $row['rank'];?></td>
                   <td Style="padding-left:20px;"><?php echo $row['username'];?></td>
                   <td Style="padding-left:20px;"><?php echo $row['groupname'];?></td>
                   <td><?php echo $row['time']; ?></td>
                 </tr>
					<?php }}?>
               </table>

               <center>
               <p><a href=".php"><button class="download"> Download </button></a></p>
               </center>

              </div>
              </div><!-- tab-content -->

 </div>
 <ul class="tab-group">
 <li class="tab active"><a href="#signup">All</a></li>
 <li class="tab"><a href="#login">Group</a></li>
 </ul>
 <center>
 <p><a href="MainMenu.php"><button class="backhome"> Back </button></a></p>
 </center>
 </div>
    </div><!-- /#main -->



    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>


 </body>
</html>
