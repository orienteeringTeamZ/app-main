-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017 年 10 朁E13 日 01:27
-- サーバのバージョン： 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orienteering`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `controlpoint`
--

CREATE TABLE `controlpoint` (
  `pointID` int(10) UNSIGNED NOT NULL,
  `eventID` int(10) DEFAULT NULL,
  `pointName` varchar(10) DEFAULT NULL,
  `pointCode` varchar(4) DEFAULT NULL,
  `lineOrder` int(2) DEFAULT NULL,
  `scatterOrder` int(2) DEFAULT NULL,
  `scorePoint` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `controlpoint`
--

INSERT INTO `controlpoint` (`pointID`, `eventID`, `pointName`, `pointCode`, `lineOrder`, `scatterOrder`, `scorePoint`) VALUES
(1001, 50002, 'A', 'p1', 1, 14, NULL),
(1002, 50002, 'B', 'p2', 2, 0, NULL),
(1003, 50002, 'C', 'p3', 3, 0, NULL),
(1004, 50002, 'D', 'p4', 4, 0, NULL),
(1005, 50002, 'E', 'p5', 5, 0, NULL),
(1006, 50002, 'F', 'p6', 6, 0, NULL),
(1007, 50002, 'G', 'p7', 7, 0, NULL),
(1008, 50002, 'H', 'p8', 8, 0, NULL),
(1009, 50002, 'I', 'p9', 9, 0, NULL),
(1010, 50002, 'J', 'p10', 10, 0, NULL),
(1011, 50002, 'K', 'p11', 11, 0, NULL),
(1012, 50002, 'L', 'p12', 12, 0, NULL),
(1013, 50002, 'M', 'p13', 13, 0, NULL),
(1014, 50002, 'N', 'p14', 14, 0, NULL),
(1015, 50002, 'O', 'p15', 15, 0, NULL),
(1016, 50002, 'P', 'p16', 16, 0, NULL),
(1017, 50002, 'Q', 'p17', 17, 0, NULL),
(1018, 50002, 'R', 'p18', 18, 0, NULL),
(1019, 50002, 'S', 'p19', 19, 0, NULL),
(1020, 50002, 'T', 'p20', 20, 0, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `course`
--

CREATE TABLE `course` (
  `courseID` int(10) UNSIGNED NOT NULL,
  `courseType` varchar(50) DEFAULT NULL,
  `courseDuration` int(30) DEFAULT NULL,
  `nrOfcontrolPoint` int(2) DEFAULT NULL,
  `startCode` varchar(30) NOT NULL,
  `eventID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `course`
--

INSERT INTO `course` (`courseID`, `courseType`, `courseDuration`, `nrOfcontrolPoint`, `startCode`, `eventID`) VALUES
(11001, 'Line', 50, 20, 'CL1', 50001),
(11002, 'Scatter', 50, 20, 'CS1', 50001),
(11003, 'Score', 50, 20, 'SCO1', 50001),
(11004, 'Line', 50, 20, 'CL2', 50003),
(11005, 'Scatter', 50, 20, 'CS2', 50003),
(11101, 'Score', 50, 20, 'SCO2', 50003),
(11102, 'Line', 50, 20, 'CL2', 50002),
(11103, 'Scatter', 50, 20, 'CS3', 50002),
(11104, 'Score', 50, 20, 'SCO3', 50002);

-- --------------------------------------------------------

--
-- テーブルの構造 `coursegroup`
--

CREATE TABLE `coursegroup` (
  `groupID` int(10) UNSIGNED NOT NULL,
  `groupName` varchar(100) DEFAULT NULL,
  `courseID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `coursegroup`
--

INSERT INTO `coursegroup` (`groupID`, `groupName`, `courseID`) VALUES
(14, 'China', 11102),
(15, 'India', 11102),
(16, 'Team Z', 11102),
(17, 'Japan', 11102);

-- --------------------------------------------------------

--
-- テーブルの構造 `course_enroll`
--

CREATE TABLE `course_enroll` (
  `id` int(100) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `courseID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `course_enroll`
--

INSERT INTO `course_enroll` (`id`, `userName`, `courseID`) VALUES
(23, 'pooja', 11102),
(24, 'smit', 11102),
(41, 'kazu', 11102),
(42, 'qqq', 11102),
(43, 'Yassa', 11102),
(44, 'kakasi', 11102),
(47, 'formget', 11102);

-- --------------------------------------------------------

--
-- テーブルの構造 `event`
--

CREATE TABLE `event` (
  `eventID` int(10) UNSIGNED NOT NULL,
  `series` varchar(100) DEFAULT NULL,
  `eventName` varchar(50) DEFAULT NULL,
  `eventDate` date DEFAULT NULL,
  `eventDuration` time DEFAULT NULL,
  `note` text,
  `samplePic` blob,
  `phoneNr` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `locationID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `event`
--

INSERT INTO `event` (`eventID`, `series`, `eventName`, `eventDate`, `eventDuration`, `note`, `samplePic`, `phoneNr`, `email`, `locationID`) VALUES
(50001, 'eve1', 'Kensington', '2017-10-18', '09:00:00', '', '', NULL, NULL, 30001),
(50002, 'eve2', 'Essendon', '2017-10-17', '19:00:00', '', '', NULL, NULL, 30002),
(50003, 'eve3', 'Ascot Vale', '2017-10-19', '15:00:00', '', '', NULL, NULL, 30003),
(50004, 'eve4', 'Gilpin Gallop', '2017-10-20', '14:00:00', '', '', NULL, NULL, 30004),
(50005, 'eve5', 'Montgomery Park', '2017-10-21', '08:00:00', '', '', NULL, NULL, 30005);

-- --------------------------------------------------------

--
-- テーブルの構造 `joinedgroup`
--

CREATE TABLE `joinedgroup` (
  `id` int(11) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `groupName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `joinedgroup`
--

INSERT INTO `joinedgroup` (`id`, `userName`, `groupName`) VALUES
(15, 'kazu', 'TeamZ'),
(16, 'qqq', 'China'),
(17, 'pooja', 'India'),
(18, 'Yassa', 'Team Z'),
(19, 'kakasi', 'Japan'),
(21, 'formget', 'China'),
(22, 'formget', 'China'),
(23, 'formget', 'China'),
(24, 'formget', 'China'),
(25, 'formget', 'China'),
(26, 'formget', 'China');

-- --------------------------------------------------------

--
-- テーブルの構造 `location`
--

CREATE TABLE `location` (
  `locationID` int(11) UNSIGNED NOT NULL,
  `locationName` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `suburb` varchar(50) DEFAULT NULL,
  `melway` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `location`
--

INSERT INTO `location` (`locationID`, `locationName`, `address`, `suburb`, `melway`) VALUES
(30001, 'Kensington', 'Childres Street', 'Kensington', '2T G7/42 J4'),
(30002, 'Railway station car park', 'Rose street', 'Essendon', '28 G4'),
(30003, 'Ascot vale Leisure centre', 'Epsom road', 'Ascot Vale', '28 F10'),
(30004, 'Brunswick Park', 'Victoria street', 'Brunswick', '29 E7'),
(30005, 'Montgomery Park', 'Hilda street', 'Essemdon', '29 A4');

-- --------------------------------------------------------

--
-- テーブルの構造 `participant`
--

CREATE TABLE `participant` (
  `participantID` int(11) UNSIGNED NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `birthYear` int(4) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `participant`
--

INSERT INTO `participant` (`participantID`, `firstName`, `lastName`, `birthYear`, `userName`, `password`) VALUES
(5, 'Yunhong', 'Zhang', 2017, 'Kazu', '123'),
(6, 'fff', 'fff', 2017, 'formget', '321'),
(7, 'Pooja', 'Patel', 1994, 'pooja', '311'),
(8, 'Smit', 'Shah', 1990, 'Smit', '311'),
(9, 'David', 'Kennady', 1983, 'davidK', '423'),
(10, 'Rose', 'carol', 1990, 'rose', '411'),
(11, 'Riya', 'carol', 1970, 'riya', '4121'),
(12, 'Hinaya', 'Patel', 2001, 'hina', '4121'),
(13, 'Maryan', 'Carson', 1974, 'MAryan', '41012'),
(14, 'James', 'Carson', 1982, 'jammy', '41991'),
(15, 'qqq', 'qqq', 2017, 'qqq', 'qqq'),
(16, 'yyy', 'yyy', 2017, 'Yassa', '123'),
(17, 'kaka', 'hatake', 2017, 'kakasi', '321'),
(18, 'Alex', 'Hunter', 2018, 'alex', 'fifa');

-- --------------------------------------------------------

--
-- テーブルの構造 `participate`
--

CREATE TABLE `participate` (
  `id` int(100) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `courseID` int(10) UNSIGNED NOT NULL,
  `time` time NOT NULL,
  `pointAcquired` int(2) DEFAULT NULL,
  `insertCode` varchar(20) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `participate`
--

INSERT INTO `participate` (`id`, `userName`, `courseID`, `time`, `pointAcquired`, `insertCode`, `order`, `status`) VALUES
(480, 'kazu', 11001, '11:34:18', 0, 'CL1', 0, 'Start'),
(481, 'kazu', 11001, '11:34:32', 0, 'p1', 1, 'wrong'),
(482, 'kazu', 11001, '11:34:39', 0, 'p2', 2, 'wrong'),
(483, 'kazu', 11001, '11:35:00', 0, 'p3', 3, 'wrong'),
(484, 'kazu', 11001, '11:35:02', 0, 'p4', 4, 'wrong'),
(485, 'kazu', 11001, '11:35:04', 0, 'p5', 5, 'wrong'),
(486, 'kazu', 11001, '11:35:05', 0, 'p6', 6, 'wrong'),
(487, 'kazu', 11001, '11:35:07', 0, 'p7', 7, 'wrong'),
(488, 'kazu', 11001, '11:35:09', 0, 'p8', 8, 'wrong'),
(489, 'kazu', 11001, '11:35:12', 0, 'p9', 9, 'wrong'),
(490, 'kazu', 11001, '11:35:19', 0, 'p10', 10, 'wrong'),
(491, 'kazu', 11001, '11:35:21', 0, 'p11', 11, 'wrong'),
(492, 'kazu', 11001, '11:35:24', 0, 'p12', 12, 'wrong'),
(493, 'kazu', 11001, '11:35:27', 0, 'p13', 13, 'wrong'),
(494, 'kazu', 11001, '11:35:28', 0, 'p14', 14, 'wrong'),
(495, 'kazu', 11001, '11:35:30', 0, 'p15', 15, 'wrong'),
(496, 'kazu', 11001, '11:35:32', 0, 'p16', 16, 'wrong'),
(497, 'kazu', 11001, '11:35:34', 0, 'p17', 17, 'wrong'),
(498, 'kazu', 11001, '11:35:36', 0, 'p18', 18, 'wrong'),
(499, 'kazu', 11001, '11:35:38', 0, 'p19', 19, 'wrong'),
(500, 'kazu', 11001, '11:35:40', 0, 'p20', 20, 'fail'),
(501, 'kazu', 11001, '11:41:25', 0, 'CL1', 21, 'wrong'),
(502, 'kazu', 11001, '11:41:30', 0, 'p1', 22, 'wrong'),
(503, 'kazu', 11001, '11:41:33', 0, 'p2', 23, 'wrong'),
(504, 'kazu', 11001, '11:41:35', 0, 'p3', 24, 'wrong'),
(505, 'kazu', 11001, '11:41:37', 0, 'p4', 25, 'wrong'),
(506, 'kazu', 11001, '11:41:38', 0, 'p5', 26, 'wrong'),
(507, 'kazu', 11001, '11:41:40', 0, 'p6', 27, 'wrong'),
(508, 'kazu', 11001, '11:41:41', 0, 'p7', 28, 'wrong'),
(509, 'kazu', 11001, '11:41:42', 0, 'p8', 29, 'wrong'),
(510, 'kazu', 11001, '11:41:44', 0, 'p9', 30, 'wrong'),
(511, 'kazu', 11001, '11:41:45', 0, 'p10', 31, 'wrong'),
(512, 'kazu', 11001, '11:41:47', 0, 'p11', 32, 'wrong'),
(513, 'kazu', 11001, '11:41:48', 0, 'p12', 33, 'wrong'),
(514, 'kazu', 11001, '11:41:50', 0, 'p13', 34, 'wrong'),
(515, 'kazu', 11001, '11:41:51', 0, 'p14', 35, 'wrong'),
(516, 'kazu', 11001, '11:41:52', 0, 'p15', 36, 'wrong'),
(517, 'kazu', 11001, '11:41:54', 0, 'p16', 37, 'wrong'),
(518, 'kazu', 11001, '11:41:55', 0, 'p17', 38, 'wrong'),
(519, 'kazu', 11001, '11:41:57', 0, 'p18', 39, 'wrong'),
(520, 'kazu', 11001, '11:41:59', 0, 'p19', 40, 'wrong'),
(521, 'kazu', 11001, '11:42:00', 0, 'p20', 41, 'wrong'),
(522, 'kazu', 11001, '11:48:47', 0, 'CL1', 0, 'Start'),
(523, 'kazu', 11001, '11:48:49', 0, 'p1', 1, 'wrong'),
(524, 'kazu', 11001, '11:48:51', 0, 'p2', 2, 'wrong'),
(525, 'kazu', 11001, '11:48:52', 0, 'p3', 3, 'wrong'),
(526, 'kazu', 11001, '11:48:53', 0, 'p4', 4, 'wrong'),
(527, 'kazu', 11001, '11:48:55', 0, 'p5', 5, 'wrong'),
(528, 'kazu', 11001, '11:48:57', 0, 'p6', 6, 'wrong'),
(529, 'kazu', 11001, '11:48:58', 0, 'p7', 7, 'wrong'),
(530, 'kazu', 11001, '11:48:59', 0, 'p8', 8, 'wrong'),
(531, 'kazu', 11001, '11:49:00', 0, 'p9', 9, 'wrong'),
(532, 'kazu', 11001, '11:49:02', 0, 'p10', 10, 'wrong'),
(533, 'kazu', 11001, '11:49:03', 0, 'p11', 11, 'wrong'),
(534, 'kazu', 11001, '11:49:04', 0, 'p12', 12, 'wrong'),
(535, 'kazu', 11001, '11:49:06', 0, 'p13', 13, 'wrong'),
(536, 'kazu', 11001, '11:49:08', 0, 'p14', 14, 'wrong'),
(537, 'kazu', 11001, '11:49:10', 0, 'p15', 15, 'wrong'),
(538, 'kazu', 11001, '11:49:12', 0, 'p16', 16, 'wrong'),
(539, 'kazu', 11001, '11:49:14', 0, 'p17', 17, 'wrong'),
(540, 'kazu', 11001, '11:49:15', 0, 'p18', 18, 'wrong'),
(541, 'kazu', 11001, '11:49:17', 0, 'p19', 19, 'wrong'),
(542, 'kazu', 11001, '11:49:19', 0, 'p20', 20, 'fail'),
(585, 'Kazu', 11001, '12:21:16', 0, 'CL1', 0, 'Start'),
(586, 'Kazu', 11001, '12:21:37', 0, 'p1', 1, 'wrong'),
(587, 'Kazu', 11001, '12:21:44', 0, 'p2', 2, 'wrong'),
(588, 'Kazu', 11001, '12:21:46', 0, 'p3', 3, 'wrong'),
(589, 'Kazu', 11001, '12:21:47', 0, 'p4', 4, 'wrong'),
(590, 'Kazu', 11001, '12:21:49', 0, 'p5', 5, 'wrong'),
(591, 'Kazu', 11001, '12:21:50', 0, 'p6', 6, 'wrong'),
(592, 'Kazu', 11001, '12:21:51', 0, 'p7', 7, 'wrong'),
(593, 'Kazu', 11001, '12:21:53', 0, 'p8', 8, 'wrong'),
(594, 'Kazu', 11001, '12:21:54', 0, 'p9', 9, 'wrong'),
(595, 'Kazu', 11001, '12:21:55', 0, 'p10', 10, 'wrong'),
(596, 'Kazu', 11001, '12:21:57', 0, 'p11', 11, 'wrong'),
(597, 'Kazu', 11001, '12:21:58', 0, 'p12', 12, 'wrong'),
(598, 'Kazu', 11001, '12:21:59', 0, 'p13', 13, 'wrong'),
(599, 'Kazu', 11001, '12:22:01', 0, 'p14', 14, 'wrong'),
(600, 'Kazu', 11001, '12:22:03', 0, 'p15', 15, 'wrong'),
(601, 'Kazu', 11001, '12:22:06', 0, 'p16', 16, 'wrong'),
(602, 'Kazu', 11001, '12:22:08', 0, 'p17', 17, 'wrong'),
(603, 'Kazu', 11001, '12:22:09', 0, 'p18', 18, 'wrong'),
(604, 'Kazu', 11001, '12:22:10', 0, 'p19', 19, 'wrong'),
(605, 'Kazu', 11001, '12:22:14', 0, 'p20', 20, 'fail'),
(606, 'Kazu', 11001, '12:22:45', 0, 'CL1', 21, 'wrong'),
(607, 'Kazu', 11001, '12:22:50', 0, 'p1', 22, 'wrong'),
(608, 'Kazu', 11001, '12:22:52', 0, 'p2', 23, 'wrong'),
(609, 'Kazu', 11001, '12:22:54', 0, 'p3', 24, 'wrong'),
(610, 'Kazu', 11001, '12:22:56', 0, 'p4', 25, 'wrong'),
(611, 'Kazu', 11001, '12:22:57', 0, 'p5', 26, 'wrong'),
(612, 'Kazu', 11001, '12:22:59', 0, 'p6', 27, 'wrong'),
(613, 'Kazu', 11001, '12:23:01', 0, 'p7', 28, 'wrong'),
(614, 'Kazu', 11001, '12:23:03', 0, 'p8', 29, 'wrong'),
(615, 'Kazu', 11001, '12:23:04', 0, 'p9', 30, 'wrong'),
(616, 'Kazu', 11001, '12:23:06', 0, 'p10', 31, 'wrong'),
(617, 'Kazu', 11001, '12:23:07', 0, 'p11', 32, 'wrong'),
(618, 'Kazu', 11001, '12:23:10', 0, 'p12', 33, 'wrong'),
(619, 'Kazu', 11001, '12:23:12', 0, 'p13', 34, 'wrong'),
(620, 'Kazu', 11001, '12:23:14', 0, 'p14', 35, 'wrong'),
(621, 'Kazu', 11001, '12:23:16', 0, 'p15', 36, 'wrong'),
(622, 'Kazu', 11001, '12:23:19', 0, 'p16', 37, 'wrong'),
(623, 'Kazu', 11001, '12:23:21', 0, 'p17', 38, 'wrong'),
(624, 'Kazu', 11001, '12:23:23', 0, 'p18', 39, 'wrong'),
(625, 'Kazu', 11001, '12:23:25', 0, 'p19', 40, 'wrong'),
(626, 'Kazu', 11001, '12:23:28', 0, 'p20', 41, 'wrong');

-- --------------------------------------------------------

--
-- テーブルの構造 `ranking`
--

CREATE TABLE `ranking` (
  `id` int(11) NOT NULL,
  `courseID` int(50) NOT NULL,
  `groupName` varchar(50) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `time` time NOT NULL,
  `score` int(10) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `ranking`
--

INSERT INTO `ranking` (`id`, `courseID`, `groupName`, `userName`, `time`, `score`, `status`) VALUES
(9, 11102, 'Hello', 'pooja', '00:00:35', 0, 'success'),
(10, 11102, '', 'Smit', '00:00:40', 0, 'success'),
(13, 11102, 'Hello', 'kazu', '00:00:33', 0, 'success'),
(14, 11102, 'Hello', 'kazu', '00:00:33', 0, 'success'),
(15, 11102, 'Hello', 'kazu', '00:00:33', 0, 'success'),
(16, 11102, 'Hello', 'Kazu', '00:00:33', 0, 'success');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `controlpoint`
--
ALTER TABLE `controlpoint`
  ADD PRIMARY KEY (`pointID`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`courseID`);

--
-- Indexes for table `coursegroup`
--
ALTER TABLE `coursegroup`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `course_enroll`
--
ALTER TABLE `course_enroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`eventID`);

--
-- Indexes for table `joinedgroup`
--
ALTER TABLE `joinedgroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`locationID`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`participantID`);

--
-- Indexes for table `participate`
--
ALTER TABLE `participate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coursegroup`
--
ALTER TABLE `coursegroup`
  MODIFY `groupID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `course_enroll`
--
ALTER TABLE `course_enroll`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `joinedgroup`
--
ALTER TABLE `joinedgroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `participantID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `participate`
--
ALTER TABLE `participate`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=648;
--
-- AUTO_INCREMENT for table `ranking`
--
ALTER TABLE `ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
